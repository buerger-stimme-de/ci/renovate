# Renovate
A tool creating merge requests for dependency updates automatically.

This project hosts the configuration and a scheduled pipeline only.

## Merge Requests
As the variables mentioned below are protected, feature branches will never build due to missing tokens.

## Configuration
- merge requests are automatically merged in case they are patches or minor updates. If the pipeline is green, the MR is merged.
- merge requests for major updates are automatically assigned to `kayman-mk` or `buerger` for review and manual merge.

### Variables
- GITLAB_TOKEN: A token which is used by Renovate to create the merge requests
- GITHUB_TOKEN: A token to access Github to avoid rate limiting fetching release information to pimp the MRs with
