TEMPLATE: Thank you for contributing to the project! Your work is highly appreciated.

          The content of the merge request is used to generate the release documentation and is also used as the commit
          description for the `master` commit.

          Please remove the TEMPLATE sections from the final merge request.

TEMPLATE: The merge request title must follow the conventional commit specification (enforced by the pipeline)
            - one sentence only (without a dot at the end)
            - don't capitalize the first letter
            - use imperative, e.g. "change" instead of "changed"

          Prefixes used:
            - feat: adds new functionality    - test:  adds tests only                    - build: changes the build system
            - fix:  fixes a bug, something    - docs:  adds documentation only            - ci:    changes the CI scripts
                    which is broken           - style: leaves the meaning of the code     - chore: necessary work like
            - perf: improves the performance           unchanged but corrects formatting,          dependency updates or
                    only                               whitespaces, ...                            changes to `.gitignore`
            - refactor: does not fix a bug nor adds a new feature and no change from below
            - revert:   reverts changes from another merge request

          Examples:
            - feat: add the save file button               - chore: bump log4j dependency to 2.17.1
            - docs: amend documentation for "register      - style: remove empty lines in package abc
                    new user" use case  

# Description
TEMPLATE: Please describe what you did and why you created the merge request, e.g. which
          feature do you implement, which bug you fix, ... Anything which is helpful
          for the reviewer to get the context.

# Test Plan
TEMPLATE: Describe how to test your change, what you did to test it. Remember: Automatic
          tests are necessary and preferred.

# Issues
TEMPLATE: Mention the issues, merge requests, ... you are referring to. Use `Closes #ISSUE_NUMBER`
          to automatically close the referenced ticket as soon as the merge request
          is merged.

          If there are not issues to mention, remove the section.

# Checklist
TEMPLATE: Use `~~` to strike through the items which are not relevant here.

- [ ] Automatic tests added
- [ ] Documentation amended
- [ ] Issues for follow ups/further enhancements created
