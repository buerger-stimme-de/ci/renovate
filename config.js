module.exports = {
  extends: ['config:base'],
  autodiscover: true,
  autodiscoverFilter: "buerger-stimme-de/**",
  reviewers: ['buerger'],
  reviewersSampleSize: 1,
  baseBranches: ['master', 'main'],
  labels: ['dependencies'],
  onboarding: false,
  gitAuthor: 'CI <ci@bürger-stimme.de>',
  printConfig: true,
  gitLabIgnoreApprovals: true,
  platformAutomerge: true,
  rebaseWhen: 'behind-base-branch',
  hostRules: [
  ],
  packageRules: [
    {
      matchUpdateTypes: ['patch', 'minor'],
      automerge: true
    },
    {
      "matchPackagePatterns": ["eslint"],
      "groupName": "eslint"
    },
    {
      "matchPackagePatterns": ["microsoft"],
      "groupName": "microsoft"
    },
    {
      "matchPackagePatterns": ["jquery"],
      "groupName": "jquery"
    }
  ]
};
